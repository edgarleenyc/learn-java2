
public class PowTable {
    public static void main(String[] args) {

        for (int i = 1; i <= 10; i = i + 1) {
			int power = 1;
            for (int j = 1; j <= 8; j = j + 1) {
				power = power * i;
                System.out.print(power + "\t");
            }
            System.out.println("");
        }
    }
}
