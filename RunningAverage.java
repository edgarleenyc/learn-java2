
public class RunningAverage {
    public static void main(String[] args) {
        java.util.Scanner scanner = new java.util.Scanner(System.in);
		
		double number = 0.0;
		double sum = 0.0;
		double average = 0.0;
		double average2 = 0.0;
		int count = 0;
		double smallest = 0.0;
		double largest = 0.0;
		
		System.out.println("\t\t\tEntered\tSmall\tLarge\tSum\tAverage\tAverage2");
		
		while(true){
			count = count + 1;
			
			System.out.println("please enter a number");
			number = scanner.nextDouble();
			
			if (number == 0) {
				break;
			}
			//int smallest and largest
			if (count == 1) {
				smallest = number;
				largest = number;
			}
			else {
				if (number < smallest) {
					smallest = number;

				}	

				if (number > largest) {
					largest = number;

				}
			}
			sum = sum + number;
			average = sum / count;
			average2 = (average2) + ((number - average2) / count);
			
			System.out.println("\t\t\t" + number + "\t" + smallest + 
					"\t" + largest + "\t" + sum + "\t" + average + "\t" + average2);
		}
		scanner.close();
		}
    }
