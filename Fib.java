
public class Fib {
    public static void main(String[] args) {
       
        int f0 = 0;
        int f1 = 1;
        int sum = 0;

        System.out.println("0:" + f0); 
        System.out.println("1:" + f1);

        for (int i = 2; i < 20; i = i + 1) {
            sum = f0 + f1;
			System.out.println(i + ":" + sum);
            f0 = f1;
            f1 = sum;
        }
    }
}
