
public class FibGraph {
    public static void main(String[] args) {
       
        int f0 = 0;
        int f1 = 1;
        int sum = 0;

        System.out.println(""); 
        System.out.println("*");

        for (int i = 2; i < 10; i = i + 1) {
            sum = f0 + f1;
            f0 = f1;
            f1 = sum;
			for (int j = 0; j < sum; j = j + 1) {
				System.out.print("*");
			}
			System.out.println();
        }
    }
}
