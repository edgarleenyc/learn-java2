

public class EmployeeMenu2 {
    public static void main(String[] args) {
        java.util.Scanner scanner = new java.util.Scanner(System.in);

        int option = 0;

        while(true) {

            System.out.println("                   Employee File");
            System.out.println("                   -------------");
            System.out.println("         Option  Description");
            System.out.println("         ------  -------------------------");
            System.out.println("            0    Exit");
            System.out.println("            1    Enter a new employee");
            System.out.println("            2    View an existing employee");
            System.out.println("            3    Delete an ex-employee");
            System.out.println("            4    List all employees");
            System.out.println();
            System.out.print("          Please choose an option: ");

            option = scanner.nextInt();

            if (option == 0) {
                System.out.print("You entered option number " + option + " to Exit");
                break;

            }

         switch (option) {
            case 1:
             System.out.println("You entered option number " + option + " to enter a new employee.");
              break;
            case 2:
              System.out.println("You entered option number " + option + " to view an existing employee.");
              break;
            case 3:
              System.out.println("You entered option number " + option + " to delete an ex-employee.");
              break;
            case 4:
               System.out.println("You entered option number " + option + " to list all employees.");
               break;
        }
        
        }
        scanner.close();

    }
}
