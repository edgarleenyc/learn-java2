
public class Mult2 {
	public static void main(String[] args) {
		
		int power = 1;
		for (int i = 1; i <= 16; i = i + 1) {
			int multiple = 2 * i;
			power = power * 2;
			System.out.println(i + "\t" + multiple + "\t" + power);
		}
	}
}