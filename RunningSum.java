
public class RunningSum {
    public static void main(String[] args) {
        java.util.Scanner scanner = new java.util.Scanner(System.in);
		
		double number = 0.0;
		double sum = 0.0;
		double average = 0.0;
		int count = 0;
		
		System.out.println("\t\t\t\t\tEntered\tSum\tAverage");
		
		while(true){
			count = count + 1;
			
			System.out.println("please enter a number");
			number = scanner.nextDouble();
			
			if (number == 0) {
				break;
			}
			sum = sum + number;
			average = sum / count;
			
			System.out.println("\t\t\t\t\t" + number + "\t" + sum 
				+ "\t" + average);
		}
		scanner.close();
		}
    }
